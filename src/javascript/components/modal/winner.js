import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import App from '../../app';

export function showWinnerModal(fighter) {
  // call showModal function
  const title = `${fighter.name} WINS!`;
  const winnerView = createWinnerView(fighter);
  const onclose = () => {
    App.rootElement.innerHTML = '';
    new App();
  };
  showModal({ title: title, bodyElement: winnerView, onClose: onclose });
}

export function createWinnerView(fighter) {
  const { name } = fighter;
  const attributes = {
    src: '../../resources/logo.png',
    title: name,
    alt: name
  };

  return createElement({
    tagName: 'img',
    className: 'modal-body-img',
    attributes
  });
}
