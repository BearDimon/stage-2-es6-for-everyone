import { controls } from '../../constants/controls';

const CRITICAL_TIMEOUT = 10000;

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over

    let firstFighterFight = { ...firstFighter, currentHealth: firstFighter.health };
    let secondFighterFight = { ...secondFighter, currentHealth: secondFighter.health };

    let keyMap = new Map();

    document.onkeyup = keyUp(keyMap, firstFighterFight, secondFighterFight, resolve);
    document.onkeydown = keyDown(keyMap);

  });
}

const keyUp = (keyMap, firstFighter, secondFighter, resolve, timers = [null, null]) => (event) => {
  let firstHealth = firstFighter.currentHealth;
  let secondHealth = secondFighter.currentHealth;

  const code = event.code;
  switch (code) {
    case controls.PlayerOneBlock:
    case controls.PlayerTwoBlock:
      keyMap.set(code, false);
      return;
    case controls.PlayerOneAttack:
      if (!keyMap.get(controls.PlayerOneBlock)
        && !keyMap.get(controls.PlayerTwoBlock)) {
        secondHealth -= getDamage(firstFighter, secondFighter);
      }
      break;
    case controls.PlayerTwoAttack :
      if (!keyMap.get(controls.PlayerTwoBlock)
        && !keyMap.get(controls.PlayerOneBlock)) {
        firstHealth -= getDamage(secondFighter, firstFighter);
      }
      break;
    default:
      if (controls.PlayerOneCriticalHitCombination.some(c => c == code)
        && controls.PlayerOneCriticalHitCombination.every(c => keyMap.get(c))
        && !timers[0]) {
        secondHealth -= getCriticalDamage(firstFighter);
        timers[0] = setTimeout(() => timers[0] = null, CRITICAL_TIMEOUT);
      } else if (controls.PlayerTwoCriticalHitCombination.some(c => c == code)
        && controls.PlayerTwoCriticalHitCombination.every(c => keyMap.get(c))
        && !timers[1]) {
        firstHealth -= getCriticalDamage(secondFighter);
        timers[1] = setTimeout(() => timers[1] = null, CRITICAL_TIMEOUT);
      }
      break;
  }

  keyMap.set(code, false);

  firstHealth = Math.max(0, firstHealth);
  secondHealth = Math.max(0, secondHealth);

  firstFighter.currentHealth = firstHealth;
  secondFighter.currentHealth = secondHealth;

  updateHealthBar(firstHealth / firstFighter.health, secondHealth / secondFighter.health);

  if (!firstHealth || !secondHealth) {
    clearTimeout(timers[0]);
    clearTimeout(timers[1]);
    resolve(!firstHealth ? secondFighter : firstFighter);
  }

};

const keyDown = (keyMap) => (event) => {
  keyMap.set(event.code, true);
};

export function updateHealthBar(firstHealthRate, secondHealthRate) {
  document.getElementById('left-fighter-indicator').style.width = `${firstHealthRate * 100}%`;
  document.getElementById('right-fighter-indicator').style.width = `${secondHealthRate * 100}%`;
}

export function getCriticalDamage(fighter) {
  return 2 * fighter.attack;
}

export function getDamage(attacker, defender) {
  // return damage
  const power = getHitPower(attacker) - getBlockPower(defender);
  return power < 0 ? 0 : power;
}

export function getHitPower(fighter) {
  // return hit power
  return fighter.attack * (Math.random() + 1);
}

export function getBlockPower(fighter) {
  // return block power
  return fighter.defense * (Math.random() + 1);
}
