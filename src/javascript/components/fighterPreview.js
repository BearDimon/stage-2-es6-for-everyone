import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`
  });

  // todo: show fighter info (name, health, etc.)
  if (fighter === undefined)
    return fighterElement;

  const fighterName = createFighterName(fighter);
  const fighterImage = createFighterImage(fighter);
  const fighterStats = createFighterStats(fighter);

  fighterElement.append(fighterName, fighterImage, fighterStats);

  return fighterElement;
}

export function createFighterName(fighter) {
  const { name } = fighter;

  const nameElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___name'
  });

  nameElement.innerHTML = name;

  return nameElement;
}

export function createFighterStats(fighter) {
  const { attack, defense, health } = fighter;
  const statsElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___stats'
  });

  const healthElement = createElement({ tagName: 'div', className: 'fighter-preview___health' });
  const attackElement = createElement({ tagName: 'div', className: 'fighter-preview___attack' });
  const defenseElement = createElement({ tagName: 'div', className: 'fighter-preview___defense' });

  healthElement.innerHTML = health;
  attackElement.innerHTML = attack;
  defenseElement.innerHTML = defense;

  statsElement.append(healthElement, attackElement, defenseElement);

  return statsElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  return createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes
  });
}
